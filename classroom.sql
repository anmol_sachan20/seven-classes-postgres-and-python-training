--
-- PostgreSQL database dump
--

-- Dumped from database version 13.3
-- Dumped by pg_dump version 13.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: availability; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.availability (
    teacher_id bigint,
    day text,
    time_slot text
);


ALTER TABLE public.availability OWNER TO postgres;

--
-- Name: batch; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.batch (
    batch_id integer NOT NULL,
    batch_name text NOT NULL
);


ALTER TABLE public.batch OWNER TO postgres;

--
-- Name: batch_batch_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.batch_batch_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.batch_batch_id_seq OWNER TO postgres;

--
-- Name: batch_batch_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.batch_batch_id_seq OWNED BY public.batch.batch_id;


--
-- Name: booked_slots; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.booked_slots (
    teacher_id bigint NOT NULL,
    day text NOT NULL,
    time_slot text NOT NULL,
    student_id bigint NOT NULL
);


ALTER TABLE public.booked_slots OWNER TO postgres;

--
-- Name: student; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.student (
    id bigint NOT NULL,
    name character varying(60) NOT NULL,
    email character varying(80) NOT NULL,
    dob date NOT NULL,
    batch_id integer NOT NULL
);


ALTER TABLE public.student OWNER TO postgres;

--
-- Name: student_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.student_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.student_id_seq OWNER TO postgres;

--
-- Name: student_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.student_id_seq OWNED BY public.student.id;


--
-- Name: teacher; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.teacher (
    id bigint NOT NULL,
    name character varying(60) NOT NULL,
    email character varying(80) NOT NULL,
    dob date NOT NULL,
    subject text NOT NULL
);


ALTER TABLE public.teacher OWNER TO postgres;

--
-- Name: teacher_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.teacher_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.teacher_id_seq OWNER TO postgres;

--
-- Name: teacher_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.teacher_id_seq OWNED BY public.teacher.id;


--
-- Name: batch batch_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.batch ALTER COLUMN batch_id SET DEFAULT nextval('public.batch_batch_id_seq'::regclass);


--
-- Name: student id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.student ALTER COLUMN id SET DEFAULT nextval('public.student_id_seq'::regclass);


--
-- Name: teacher id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teacher ALTER COLUMN id SET DEFAULT nextval('public.teacher_id_seq'::regclass);


--
-- Data for Name: availability; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.availability (teacher_id, day, time_slot) FROM stdin;
1	monday	09-18
1	tuesday	10-17
1	wednesday	10-15
1	thursday	11-18
1	friday	09-16
1	saturday	10-17
2	monday	09-17
2	tuesday	09-16
2	wednesday	10-18
2	thursday	11-15
2	friday	10-16
2	saturday	09-15
\.


--
-- Data for Name: batch; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.batch (batch_id, batch_name) FROM stdin;
1	CBSE 10
2	ICSE 10
3	NEET Droppers
4	JEE Droppers
5	CBSE 12 PCM
6	CBSE 12 PCB
7	ICSE 12 PCM
8	ICSE 12 PCB
9	CBSE 11 PCM
10	ICSE 11 PCM
11	CBSE 12 PCB
12	ICSE 12 PCB
\.


--
-- Data for Name: booked_slots; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.booked_slots (teacher_id, day, time_slot, student_id) FROM stdin;
1	monday	9-10	1
2	saturday	13-14	6
1	tuesday	11-12	7
2	friday	12-13	7
1	monday	16-17	7
2	wednesday	16-17	9
\.


--
-- Data for Name: student; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.student (id, name, email, dob, batch_id) FROM stdin;
6	Anmol	anmol@abc.com	2005-08-20	10
7	Abhishek	abhishek@abc.com	2006-10-04	9
9	Aman	aman@abc.com	2001-06-03	6
\.


--
-- Data for Name: teacher; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.teacher (id, name, email, dob, subject) FROM stdin;
1	Tanuj	tanuj@xyz.com	1988-12-20	Physics
2	Aman	aman@xyz.com	1984-06-18	Chemistry
3	Dinesh	dinesh@7cls.com	1983-06-30	Maths
\.


--
-- Name: batch_batch_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.batch_batch_id_seq', 25, true);


--
-- Name: student_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.student_id_seq', 9, true);


--
-- Name: teacher_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.teacher_id_seq', 3, true);


--
-- Name: batch batch_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.batch
    ADD CONSTRAINT batch_pkey PRIMARY KEY (batch_id);


--
-- Name: student student_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.student
    ADD CONSTRAINT student_pkey PRIMARY KEY (id);


--
-- Name: teacher teacher_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teacher
    ADD CONSTRAINT teacher_pkey PRIMARY KEY (id);


--
-- Name: availability availability_teacher_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.availability
    ADD CONSTRAINT availability_teacher_id_fkey FOREIGN KEY (teacher_id) REFERENCES public.teacher(id) NOT VALID;


--
-- Name: booked_slots booked_slots_student_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.booked_slots
    ADD CONSTRAINT booked_slots_student_id_fkey FOREIGN KEY (student_id) REFERENCES public.student(id) NOT VALID;


--
-- Name: booked_slots booked_slots_teacher_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.booked_slots
    ADD CONSTRAINT booked_slots_teacher_id_fkey FOREIGN KEY (teacher_id) REFERENCES public.teacher(id) NOT VALID;


--
-- Name: student student_batch_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.student
    ADD CONSTRAINT student_batch_id_fkey FOREIGN KEY (batch_id) REFERENCES public.batch(batch_id) NOT VALID;


--
-- PostgreSQL database dump complete
--

